<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="css/layout/layout.css">--}}
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <style>
        .app-title {
            color:white !important;
            font-style: normal;
            font-family: "Arial Sans";
            font-weight: 800;
            text-transform: uppercase;

        }
        .searchbar_input {
            background-color: #f2f2f2;
            border: 1px solid #f2f2f2;
            height: 40px;
            margin-top:10px;
            width: 100%;
            padding-left:20px;
        }
        .create-button {
            color:white;
            background-color: #001c33 ;
            border-radius: 5px;
            font-size:14px;
            padding: 8px 50px;
            /*margin: 30px 205px 20px;*/
            border: 2px solid #001c33 ;
        }
        .mybreadcrumb li {
            margin-left: 160px;
            padding: 10px 50px 10px 0px;
            font-size: 20px;
            font-weight: 500;
            color: black !important;
        }
        .mybreadcrumb {
            margin-top:80px;
            height:70px;
            width: 100%;
        }
        .comment-post {
            background-color: #f2f2f2;
            border: 1px solid #f2f2f2;
        }
        .show-content {
            -moz-hyphens:auto !important;
            -ms-hyphens:auto !important;
            -webkit-hyphens:auto !important;
            hyphens:auto !important;
            word-wrap:break-word !important;
        }
        .show-purpose {
            -moz-hyphens:auto !important;
            -ms-hyphens:auto !important;
            -webkit-hyphens:auto !important;
            hyphens:auto !important;
            word-wrap:break-word !important;
        }
        .show-comment-content {
            -moz-hyphens:auto !important;
            -ms-hyphens:auto !important;
            -webkit-hyphens:auto !important;
            hyphens:auto !important;
            word-wrap:break-word !important;
            padding-left: 0px;
        }
        .show-comment-post {
            padding-left: 0px;
        }
        .show-comment-number {
            height: 40px;
        }
        .show-comment-header {
            height: 50px !important;
            padding-left: 0px;
        }
        .detailsbutton {
            color:white;
            background-color: #001c33 ;
            border-radius: 5px;
            font-size:14px;
            padding: 8px 20px;
            border: 2px solid #001c33 ;
        }
        .detailsbutton:hover {
            background: #4a97c2;
            border: 2px solid #4a97c2;
        }
        body h2 ,h1 {
            color: #001c33;

        }

        h3, h4, h5, h6 {
            color: #093459;

        }
         p{
             color: #093459;
             font-family: "Open Sans";
             font-style: normal;
             font-weight:400;
         }
        .describe {

        }
        .item {
            padding: 0 20px 20px;
        }

        .item-box {
            border: 1px solid #eee;
            height:400px;
            margin-top: 10px;
            -moz-hyphens:auto !important;
            -ms-hyphens:auto !important;
            -webkit-hyphens:auto !important;
            hyphens:auto !important;
            word-wrap:break-word !important;
        }

        .layoutnav {
            background-color: #093459 !important;
            color: #fff0ef !important;
            position: relative !important;
            padding-top: 10px !important;
            padding-bottom: 10px !important;
            height: 80px !important;
            position: fixed !important;
            top: 0px !important;
            overflow: auto;
        }
        span:hover {
            background-color: #1785e4;
        }
        span {
            display: inline-block;
            line-height: 1;
            padding: 0.5em 11px;
            padding-top: 0.5em;
            padding-right: 11px;
            padding-bottom: 0.5em;
            padding-left: 11px;
            border-radius: 3px;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }
        li {

        }
        li a {
            color:white !important;
            text-transform: uppercase;
            font-weight: 700;
            font-family: 'Helvetica Neue', Arial, sans-serif;
            font-size: 14px;
        }
    </style>

</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top layoutnav navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand app-title" href="{{ url('/transfers') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                {{--<ul class="nav navbar-nav">--}}
                    {{--&nbsp;--}}
                    {{--<li><a href="/historys"><span>History</span></a></li>--}}
                    {{--<li><a href="/transfers/my_store"><span>My store</span></a></li>--}}
                    {{--<li><a href="/transfers/create"><span>Create</span></a></li>--}}
                {{--</ul>--}}

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li><a href="/historys"><span>History</span></a></li>
                        <li><a href="/transfers/my_store"><span>My store</span></a></li>
                        <li><a href="/transfers/create"><span>Create</span></a></li>
                        <li class="dropdown" style="background-color: transparent !important;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false" style="background-color: transparent !important;">
                                <span>{{ Auth::user()->name }}</span>
                            </a>

                            <ul class="dropdown-menu" role="menu" style="background-color: #1785e4 !important;margin-top: -8px;color:white">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')


</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
