@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default" style="margin-top: 100px">
					<div class="panel-heading">
						Create
					</div>
					<div class="panel-body">
						<div class="create-form">
							<form action="/transfers" method="POST" enctype="multipart/form-data" class="form-horizontal">

								{{csrf_field()}}
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label for="title" class=" control-label" style="padding-left: 120px">Title</label>
										</div>
										<div class="col-md-6">
											<input class="form-control" style="" type="text" name="title" value="" id="title" required>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label for="describe" class="control-label" style="padding-left: 90px">Describe</label>
										</div>
										<div class="col-md-6">
											<textarea type="text" class="form-control" name="describe" value="" id="describe" required></textarea>
										</div>
									</div>


								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label for="purpose" class="control-label" style="padding-left: 93px">Purpose</label>
										</div>
										<div class="col-md-6">
											<textarea type="text" class="form-control" name="purpose" value="" id="purpose" required></textarea>
										</div>
									</div>


								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label for="image" class="control-label" style="padding-left: 108px">Image</label>
										</div>
										<div class="col-md-6">
											<input type="file" class="form-inline" name="img" value="" id="image" required>
										</div>
									</div>


								</div>


								<div class="col-md-8 col-md-offset-4">
									<input type="submit" name="submit" value="Create" class="create-button">
								</div>


							</form>
						</div>
					</div>

				</div>

			</div>
			<div class="col-md-3">

			</div>
		</div>
	</div>



@endsection