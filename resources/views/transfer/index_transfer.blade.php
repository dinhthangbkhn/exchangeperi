@extends('layouts.app')

@section('content')
    <ol class="breadcrumb mybreadcrumb">
        <li class="active">Blog</li>
    </ol>
    <div class="container" style="padding: 30px 0px 0px 0px;margin-left: 160px">

        <div class="row">
            <div class="col-md-8">
                <div class="col-md-6">

                    @for($i=0; $i<count($all)/2; $i++)
                        <div class="item-box">

                            <img src="/{{$all[$i]->imgUrl}}" style="height: 200px;width: 100%" class="item-img">

                            <div class="item">
                                <h2>{{$all[$i]->title}}</h2>
                                <p>{{$all[$i]->describe}}</p>
                                {{--<p>{{$all[$i]->imgUrl}}</p>--}}
                                <form action="/transfers/{{$all[$i]->id}}">

                                    <input type="submit" value="View detail" class="detailsbutton"/>

                                </form>



                            </div>
                        </div>

                        <br><br>
                    @endfor
                </div>
                <div class="col-md-6">

                    @for($i=count($all)/2; $i<count($all); $i++)

                        <div class="item-box">

                            <img src="/{{$all[$i]->imgUrl}}" style="height: 200px;width: 100%" class="item-img">

                            <div class="item">
                                <h2>{{$all[$i]->title}}</h2>
                                <p>{{$all[$i]->describe}}</p>
                                {{--<p>{{$all[$i]->imgUrl}}</p>--}}
                                <form action="/transfers/{{$all[$i]->id}}">

                                    <input type="submit" value="View detail" class="detailsbutton"/>

                                </form>



                            </div>
                        </div>

                        <br><br>
                    @endfor

                </div>


            </div>

            <div class="col-md-4">
                <div class="col-md-8">
                    <div class="searchbar">
                        <form method="get" action="/search/transfers/">

                            <input type="text" name="search_text" placeholder="Search" class="searchbar_input"/>
                        </form>
                    </div>
                </div>

            </div>
        </div>


    </div>


@endsection