@extends('layouts.app')
@section('content')
    <input type="hidden" name="username" value="{{Auth::user()->name}}">
    <div class="container" style="padding-top: 200px;margin-left:160px">
        <div class="row">
            <div class="col-md-8">
                <div class="show-user-item">
                    <img src="/{{$imgUrl}}" style="width: 100%;height:auto">
                    <h1>Title</h1>

                    <div class="show-content">
                        <div class="show-describe">
                            <h4>Describe</h4>
                            <p>{{$describe}}</p>
                            <br>
                        </div>
                        <div class="show-purpose">
                            <h4>Purpose</h4>
                            <p>{{$purpose}}</p>
                            <br>
                        </div>
                    </div>
                </div>
                <br>
                <div class="show-comment-item">
                    <div class="show-comment-number">
                        <div class="col-md-4" style="padding-left: 0px">
                            <div style="font-size: 18px;color:black">This post has {{count($proposes)}} comment</div>
                        </div>
                        <div class="col-md-8">
                            <hr style="color:black">
                        </div>
                    </div>


                    <div>
                        @foreach($proposes as $propose)
                            <div class="show-comment-header">
                                <div class="col-md-2">
                                    <img src="/images/user.png" style="height: 50px;width: 50px"/>
                                </div>
                                <div class="col-md-10" style="padding-left: 0px">
                                    <b>{{$propose->name}}</b>
                                </div>
                            </div>


                            <div class="show-comment-content">
                                <div class="col-md-2">

                                </div>
                                <div class="col-md-10" style="padding-left: 0px">
                                    {{$propose->propose}}
                                </div>
                            </div>
                            <br>
                            <hr>
                        @endforeach


                    </div>
                </div>

                <div class="show-comment-post" >

                        <h3>Leave your comment here</h3>
                        @if (Auth::user())
                            <form method="POST" enctype="multipart/form-data" class="form">
                                {{csrf_field()}}
                                <input type="hidden" name="transfer_id" value="{{$id}}">

                                <textarea type="text" name="propose" placeholder="" class="comment-post" maxlength="65525" style="height: 200px;width: 60%"></textarea>
                                <br>
                                <br>
                                <input type="submit" value="Propose" id="propose" class="detailsbutton">
                            </form>

                        @endif

                </div>
            </div>


        </div>
    </div>


    <br>



    {{--@if (Auth::user() == $owner)--}}
    {{--day la nut delete--}}
    {{--@endif--}}

    <script type="text/javascript">
        $(document).ready(function () {
            // Bind submit button onclick handler to send an Ajax request and
            //  process Ajax response.
            $(':submit').click(function (event) {
                event.preventDefault();  // Do not run the default action

                var submittedMessage = $(':text[name="message"]').val();
                var transfer_id = $(':hidden[name="transfer_id"]').val();
                var propose = $(':text[name="propose"]').val();
                $.ajax({
                    type: 'POST',
                    url: '/historys',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {transfer_id: transfer_id, propose: propose}
                })
                    .done(function (responseText) {
                        // Triggered if response status code is 200 (OK)
                        $('#comment').append($(':hidden[name="username"]').val() + ':' + responseText);
                    })
                    .fail(function (jqXHR, status, error) {
                        // Triggered if response status code is NOT 200 (OK)
                        alert(jqXHR.responseText);
                    })
                    .always(function () {
                        // Always run after .done() or .fail()
                        // $('p:first').after('<p>Thank you.</p>');
                    });
            });
        });
    </script>
@endsection
