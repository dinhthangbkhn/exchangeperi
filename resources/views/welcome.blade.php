<!DOCTYPE html>
<html class=" js_active  vc_desktop  vc_transform  vc_transform  vc_transform " lang="en-US">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://totaltheme.wpengine.com/webdevelopers/xmlrpc.php">
    <!-- Google Tag Manager -->
    <script async="" src="js/welcome/analytics.js"></script>
    <script type="text/javascript" async="" src="js/welcome/ec.js"></script>
    <script type="text/javascript" async=""
            src="js/welcome/analytics.js"></script>
    <script async="" src="js/welcome/gtm.js"></script>
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KS2PQ4J');</script>
    <title>WEBdevelopers – We make websites</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="Total WordPress Theme 3.6.0">
    <link rel="dns-prefetch" href="http://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="http://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="WEBdevelopers » Feed"
          href="http://totaltheme.wpengine.com/webdevelopers/feed/">
    <link rel="alternate" type="application/rss+xml" title="WEBdevelopers » Comments Feed"
          href="http://totaltheme.wpengine.com/webdevelopers/comments/feed/">
    <link rel="stylesheet" id="js_composer_front-css"
          href="css/welcome/js_composer.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpex-style-css" href="css/welcome/style.css"
          type="text/css" media="all">
    <link rel="stylesheet" id="wpex-google-font-open-sans-css"
          href="css/welcome/css.css" type="text/css" media="all">
    <link rel="stylesheet" id="wpex-visual-composer-css"
          href="css/welcome/wpex-visual-composer.css" type="text/css"
          media="all">
    <link rel="stylesheet" id="wpex-visual-composer-extend-css"
          href="css/welcome/wpex-visual-composer-extend.css" type="text/css"
          media="all">

    <link rel="stylesheet" id="wpex-responsive-css"
          href="css/welcome/wpex-responsive.css" type="text/css" media="all">

    <script type="text/javascript" src="js/welcome/jquery.js"></script>
    <script type="text/javascript"
            src="js/welcome/jquery-migrate.js"></script>
    <link rel="https://api.w.org/" href="http://totaltheme.wpengine.com/webdevelopers/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://totaltheme.wpengine.com/webdevelopers/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://totaltheme.wpengine.com/webdevelopers/wp-includes/wlwmanifest.xml">
    <link rel="canonical" href="http://totaltheme.wpengine.com/webdevelopers/">
    <link rel="shortlink" href="http://totaltheme.wpengine.com/webdevelopers/">
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="http://totaltheme.wpengine.com/webdevelopers/wp-content/plugins/js_composer/css/vc_lte_ie9.min.css"
          media="screen"><![endif]--><!--[if IE  8]>
    <link rel="stylesheet" type="text/css"
          href="http://totaltheme.wpengine.com/webdevelopers/wp-content/plugins/js_composer/css/vc-ie8.min.css'"
          media="screen"><![endif]-->
    <style type="text/css" data-type="vc_custom-css">/* Hide home link on this page */
        #menu-item-65 {
            display: none;
        }</style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1477934382013 {
            padding-top: 250px !important;
            padding-bottom: 200px !important;
            background: #093459 url(http://totaltheme.wpengine.com/webdevelopers/wp-content/uploads/sites/30/2015/02/bg.jpg?id=26) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1477932972705 {
            padding-top: 60px !important;
            padding-bottom: 30px !important;
        }

        .vc_custom_1477935598698 {
            margin-bottom: 30px !important;
        }

        .vc_custom_1477936653728 {
            border-top-width: 1px !important;
            padding-top: 80px !important;
            padding-bottom: 40px !important;
            border-top-color: #e8f2fc !important;
            border-top-style: solid !important;
        }

        .vc_custom_1477936660848 {
            border-top-width: 1px !important;
            padding-top: 80px !important;
            padding-bottom: 40px !important;
            border-top-color: #e8f2fc !important;
            border-top-style: solid !important;
        }

        .vc_custom_1477936666575 {
            border-top-width: 1px !important;
            padding-top: 80px !important;
            padding-bottom: 40px !important;
            border-top-color: #e8f2fc !important;
            border-top-style: solid !important;
        }

        .vc_custom_1477944791169 {
            border-top-width: 1px !important;
            padding-top: 80px !important;
            padding-bottom: 40px !important;
            border-top-color: #e8f2fc !important;
            border-top-style: solid !important;
        }</style>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript><!-- Google Analytics Tracking Code -->
    <script>!function (e, a, t, n, c, o, s) {
            e.GoogleAnalyticsObject = c, e[c] = e[c] || function () {
                    (e[c].q = e[c].q || []).push(arguments)
                }, e[c].l = 1 * new Date, o = a.createElement(t), s = a.getElementsByTagName(t)[0], o.async = 1, o.src = n, s.parentNode.insertBefore(o, s)
        }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-71858554-1", "auto"), ga("send", "pageview");</script>
    <style type="text/css" data-type="wpex-css">/*TYPOGRAPHY*/
        body {
            font-family: "Open Sans";
            font-size: 14px;
            color: #093459
        }

        #site-navigation .dropdown-menu a {
            font-weight: 700;
            font-size: 12px;
            letter-spacing: 2px;
            text-transform: uppercase
        }

        #site-navigation .dropdown-menu ul a {
            font-size: 12px;
            letter-spacing: 0;
            text-transform: capitalize
        }

        .blog-entry-title.entry-title a, .blog-entry-title.entry-title a:hover {
            font-weight: 600;
            font-size: 21px
        }

        h1, h2, h3, h4, h5, h6, .theme-heading, .page-header-title, .heading-typography, .widget-title, .wpex-widget-recent-posts-title, .comment-reply-title, .vcex-heading, .entry-title, .sidebar-box .widget-title, .search-entry h2 {
            color: #001c33;
            line-height: 1.25
        }

        .theme-heading {
            font-size: 18px;
            margin: 0 0 25px
        }

        .sidebar-box .widget-title {
            font-weight: 700;
            font-size: 13px;
            letter-spacing: 1px;
            text-transform: uppercase
        }

        .footer-callout-content {
            font-size: 21px
        }

        /*ADVANCED STYLING CSS*/
        .is-sticky #site-header {
            border-color: transparent
        }

        /*CUSTOMIZER STYLING*/
        #site-scroll-top {
            border-width: 2px;
            width: 30px;
            height: 30px;
            line-height: 30px;
            font-size: 14px;
            border-radius: 3px;
            right: 20px;
            bottom: 20px;
            color: #093459;
            background-color: #ffffff;
            border-color: #093459
        }

        #site-scroll-top:hover {
            color: #ffffff;
            background-color: #093459;
            border-color: #093459
        }

        a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .entry-title a:hover {
            color: #1161a6
        }

        .theme-button, input[type="submit"], button {
            background: #093459
        }

        .navbar-style-one .menu-button > a > span.link-inner:hover {
            background: #093459;
            background: #0c4678
        }

        .theme-button:hover, input[type="submit"]:hover, button:hover {
            background: #0c4678
        }

        #site-header {
            background-color: #093459
        }

        #site-header-sticky-wrapper {
            background-color: #093459
        }

        #site-header-sticky-wrapper.is-sticky #site-header {
            background-color: #093459
        }

        .footer-has-reveal #site-header {
            background-color: #093459
        }

        #searchform-header-replace {
            background-color: #093459
        }

        body.wpex-has-vertical-header #site-header {
            background-color: #093459
        }

        #site-navigation .dropdown-menu > li > a {
            color: #ffffff
        }

        #site-navigation .dropdown-menu > li > a:hover {
            color: #ffffff
        }

        #site-navigation .dropdown-menu > .current-menu-item > a, #site-navigation .dropdown-menu > .current-menu-parent > a, #site-navigation .dropdown-menu > .current-menu-item > a:hover, #site-navigation .dropdown-menu > .current-menu-parent > a:hover {
            color: #ffffff !important
        }

        #site-navigation .dropdown-menu > li > a:hover > span.link-inner {
            background-color: #1785e4
        }

        #site-navigation .dropdown-menu > .current-menu-item > a > span.link-inner, #site-navigation .dropdown-menu > .current-menu-parent > a > span.link-inner, #site-navigation .dropdown-menu > .current-menu-item > a:hover > span.link-inner, #site-navigation .dropdown-menu > .current-menu-parent > a:hover > span.link-inner {
            background-color: #1785e4
        }

        #site-header #site-navigation .dropdown-menu ul {
            background-color: #093459
        }

        #site-header #site-navigation .dropdown-menu ul > li > a {
            color: #ffffff
        }

        #site-header #site-navigation .dropdown-menu ul > li > a:hover {
            background-color: #1785e4
        }

        #mobile-menu a {
            font-size: 21px;
            color: #ffffff;
            background: #093459;
            border-color: #093459
        }

        #mobile-menu a:hover {
            color: #ffffff;
            background: #093459;
            border-color: #093459
        }

        #sidr-main {
            background-color: #001021
        }

        #sidr-main li, #sidr-main ul {
            border-color: #04192b
        }

        .sidr a, .sidr-class-dropdown-toggle {
            color: #ffffff
        }

        .sidr a:hover, .sidr-class-dropdown-toggle:hover, .sidr-class-dropdown-toggle .fa, .sidr-class-menu-item-has-children.active > a, .sidr-class-menu-item-has-children.active > a > .sidr-class-dropdown-toggle {
            color: #ffffff
        }

        #footer-callout-wrap {
            background-color: #04192b;
            border-color: #04192b;
            color: #ffffff
        }

        #footer-callout .theme-button {
            border-radius: 3px !important;
            background: #093459
        }

        #footer-callout .theme-button:hover {
            background: #0c4678
        }

        .wpex-vc-column-wrapper {
            margin-bottom: 40px
        }</style>
</head>

<!-- Begin Body -->
<body class="home page-template-default page page-id-2 wp-custom-logo wpex-theme wpex-responsive full-width-main-layout has-composer wpex-live-site content-full-screen has-breadcrumbs sidebar-widget-icons hasnt-overlay-header no-header-margin page-header-disabled smooth-fonts wpex-mobile-toggle-menu-icon_buttons has-mobile-menu wpb-js-composer js-comp-ver-4.12.1 vc_responsive wpex-window-loaded">

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KS2PQ4J"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<span data-ls_id="#site_top"></span>
<div id="outer-wrap" class="clr">


    <div id="wrap" class="clr">


        <div id="site-header-sticky-wrapper" class="wpex-sticky-header-holder is-sticky" style="height: 86px;">
            <header id="site-header" class="header-one fixed-scroll wpex-dropdown-style-minimal-sq clr"
                    itemscope="itemscope" itemtype="http://schema.org/WPHeader" style="top: 0px; width: 1303px;">


                <div id="site-header-inner" class="container clr">


                    <div id="site-logo" class="site-branding clr header-one-logo">
                        <div id="site-logo-inner" class="clr">
                            <a href="http://totaltheme.wpengine.com/webdevelopers/" title="WEBdevelopers" rel="home"
                               class="main-logo"><img
                                        src="images/welcome/logo3w.png"
                                        alt="WEBdevelopers" class="logo-img" data-no-retina=""></a>
                        </div><!-- #site-logo-inner -->
                    </div><!-- #site-logo -->

                    <div id="site-navigation-wrap" class="navbar-style-one wpex-dropdowns-caret clr">

                        <nav id="site-navigation" class="navigation main-navigation clr" itemscope="itemscope"
                             itemtype="http://schema.org/SiteNavigationElement">


                            <ul id="menu-main" class="dropdown-menu sf-menu sf-js-enabled">
                                @if (Route::has('login'))
                                    @if (Auth::check())
                                        <li id="menu-item-72"
                                            class="local-scroll menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-72">
                                            <a href="{{ url('/home') }}"><span class="link-inner">Home</span></a>
                                        </li>
                                    @else
                                        <li id="menu-item-10"
                                            class="local-scroll menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-10">
                                            <a href="{{ url('/login') }}"><span class="link-inner">Login</span></a>
                                        </li>
                                        <li id="menu-item-67"
                                            class=" menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-67">
                                            <a href="{{ url('/register') }}"><span
                                                        class="link-inner">Register</span></a>
                                        </li>
                                    @endif
                                @endif
                                <li id="menu-item-52"
                                    class="local-scroll menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-52">
                                    <a href="http://localhost:8000/#home-about"
                                       data-ls_linkto="#home-about" class=""><span
                                                class="link-inner">About Us</span></a></li>
                                <li id="menu-item-9"
                                    class="local-scroll menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-9">
                                    <a href="http://localhost:8000/#home-what"
                                       data-ls_linkto="#home-what" class=""><span
                                                class="link-inner">What We Do</span></a></li>
                                {{--<li id="menu-item-72"--}}
                                {{--class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-72">--}}
                                {{--<a href="#" class="sf-with-ul"><span class="link-inner">Features <span--}}
                                {{--class="nav-arrow top-level fa fa-angle-down"></span></span></a>--}}
                                {{--<ul class="sub-menu" style="display: none;">--}}
                                {{--<li id="menu-item-73"--}}
                                {{--class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73">--}}
                                {{--<a target="_blank" href="http://totaltheme.wpengine.com/"><span--}}
                                {{--class="link-inner">More Theme Demos</span></a></li>--}}
                                {{--<li id="menu-item-75"--}}
                                {{--class="menu-item menu-item-type-custom menu-item-object-custom menu-item-75">--}}
                                {{--<a target="_blank"--}}
                                {{--href="http://totaltheme.wpengine.com/features/builder-modules/"><span--}}
                                {{--class="link-inner">All Builder Modules</span></a></li>--}}
                                {{--<li id="menu-item-74"--}}
                                {{--class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74">--}}
                                {{--<a target="_blank"--}}
                                {{--href="http://totaltheme.wpengine.com/features/theme-features/"><span--}}
                                {{--class="link-inner">All Theme Features</span></a></li>--}}
                                {{--</ul>--}}
                                {{--</li>--}}

                            </ul>

                        </nav><!-- #site-navigation -->

                    </div><!-- #site-navigation-wrap -->


                    <div id="mobile-menu" class="clr wpex-mobile-menu-toggle wpex-hidden">
                        <a href="#" class="mobile-menu-toggle"><span class="fa fa-navicon"></span></a>
                    </div><!-- #mobile-menu -->
                </div><!-- #site-header-inner -->


            </header>
        </div><!-- #header -->


        <main id="main" class="site-main clr">


            <div id="content-wrap" class="container clr">


                <div id="primary" class="content-area clr">


                    <div id="content" class="site-content clr">


                        <article class="single-page-article wpex-clr">
                            <div class="entry clr">
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered wpex-offset-vc-15">
                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1477934382013 wpex-has-overlay">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="vcex-typed-text-wrap clr vcex-module"
                                                                 style="color:#ffffff;font-size:42px;font-weight:400;text-align:center;">
                                                                <span class="vcex-typed-text"
                                                                      data-settings="{&quot;typeSpeed&quot;:30,&quot;loop&quot;:false,&quot;showCursor&quot;:false,&quot;backDelay&quot;:500,&quot;backSpeed&quot;:&quot;0&quot;,&quot;startDelay&quot;:&quot;0&quot;}"
                                                                      data-strings="[&quot;All the best developers, at your fingertips.&quot;,&quot;Create the site of your dreams.&quot;,&quot;Don't settle for less, choose Total WP Theme.&quot;,&quot;Get started Today!&quot;]">Get started Today!</span>
                                                            </div>
                                                            <div class="vcex-spacing" style="height:40px"></div>
                                                            <div class="textcenter theme-button-wrap clr"><a
                                                                        href="http://totaltheme.wpengine.com/webdevelopers/contact/"
                                                                        class="vcex-button theme-button flat align-center animate-on-hover wpex-data-hover"
                                                                        title="Contact us"
                                                                        style="background-color: rgb(9, 52, 89) ! important; background-image: none; background-repeat: repeat; background-attachment: scroll; background-clip: border-box; background-origin: padding-box; background-position: 0% 0%; background-size: auto auto; padding: 12px 32px; font-size: 13px; font-weight: 600; letter-spacing: 1px; border-radius: 5px; text-transform: uppercase;"
                                                                        data-hover-background="#0c4678"><span
                                                                            class="theme-button-inner">Contact us</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="wpex-bg-overlay dark"
                                              style="opacity:0.35;-moz-opacity:0.35;-webkit-opacity:0.35;"></span></div>
                                </div>
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered">
                                    <div data-ls_id="#home-about"
                                         class="vc_row wpb_row vc_row-fluid vc_custom_1477932972705">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_text_column wpb_content_element "
                                                                 style="font-size:28px;">
                                                                <div class="wpb_wrapper"><p style="text-align: center;">
                                                                        Chào mọi người, từ năm 2017, chúng tôi đã phát triển dịch vụ trao đổi đồ cũ
                                                                    với phương châm, cục sh!t của người này là bát cơm của người kia</p>
                                                                </div>
                                                            </div>
                                                            <div class="vcex-spacing" style="height:30px"></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper"><p style="text-align: center;">
                                                                        Chúng tôi tạo ra hình thức thanh toán, trao đổi hàng hóa thuận tiện, đi kèm
                                                                    theo đó là hệ thống kho chưa di động, có thể có mặt tại bất cứ nơi đâu, tại bất cứ thời điểm nào.
                                                                        Mọi người có thể trao đổi hàng hóa của mình một cách thuận tiện nhất có thể. Không những thế,
                                                                        chúng tôi còn giúp nâng cao ý thức bảo vệ môi trường trong mỗi chúng ta.
                                                                        MAKE VIETNAM GREATE AGAIN!!!
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered wpex-vc-has-custom-column-spacing wpex-vc-column-spacing-50">
                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1477935598698">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="vcex-module vcex-icon-box clr vcex-icon-box-two">
                                                                <img src="images/welcome/time.png"
                                                                     class="vcex-icon-box-image" alt="Since 2002"
                                                                     style="margin-bottom:25px;" width="128"
                                                                     height="128">
                                                                <h2 class="vcex-icon-box-heading"
                                                                    style="font-size:18px;">Since 2017</h2>
                                                                <div class="vcex-icon-box-content clr"><p>Chúng tôi cung cấp những dịch vụ từ năm 2017.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="vcex-module vcex-icon-box clr vcex-icon-box-two">
                                                                <img src="images/welcome/place.png"
                                                                     class="vcex-icon-box-image" alt="Global"
                                                                     style="margin-bottom:25px;" width="128"
                                                                     height="128">
                                                                <h2 class="vcex-icon-box-heading"
                                                                    style="font-size:18px;">Global</h2>
                                                                <div class="vcex-icon-box-content clr"><p>Mục tiêu phát triển ra toàn thế giới với
                                                                    hàng tỷ người </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-4">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="vcex-module vcex-icon-box clr vcex-icon-box-two">
                                                                <img src="images/welcome/star.png"
                                                                     class="vcex-icon-box-image" alt="5 Star Design"
                                                                     style="margin-bottom:25px;" width="128"
                                                                     height="128">
                                                                <h2 class="vcex-icon-box-heading"
                                                                    style="font-size:18px;">5 Star </h2>
                                                                <div class="vcex-icon-box-content clr"><p>Luôn luôn nỗ lực 100% để
                                                                    đem lại dịch vụ tốt nhất, trải nghiệm mới lạ nhất cho khách hàng với tôn chỉ khách hàng
                                                                    là thượng đế </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered wpex-vc-has-custom-column-spacing wpex-vc-column-spacing-40 wpex-offset-vc-20">
                                    <div data-ls_id="#home-what"
                                         class="vc_row wpb_row vc_row-fluid vc_custom_1477936653728">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_text_column wpb_content_element "
                                                                 style="font-size:28px;">
                                                                <div class="wpb_wrapper"><p style="text-align: left;">
                                                                        <strong>01.</strong> We do things.</p>
                                                                </div>
                                                            </div>
                                                            <div class="vcex-spacing" style="height:30px"></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper"><p>Nullam
                                                                        euismod viverra felis. Morbi vitae eros ut enim
                                                                        congue auctor in vitae
                                                                        sem. Curabitur at ipsum eu urna condimentum
                                                                        rhoncus.
                                                                        Interdum et
                                                                        malesuada fames ac ante ipsum primis in
                                                                        faucibus.
                                                                        Maecenas viverra
                                                                        ullamcorper aliquam. Cras viverra condimentum
                                                                        libero
                                                                        in lacinia.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left wpb_start_animation">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_single_image-wrapper"><img
                                                                                src="images/welcome/mac.png"
                                                                                class="vc_single_image-img attachment-full"
                                                                                alt="" width="784" height="450"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered wpex-vc-has-custom-column-spacing wpex-vc-column-spacing-40 wpex-cols-right wpex-offset-vc-20">
                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1477936660848">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_text_column wpb_content_element "
                                                                 style="font-size:28px;">
                                                                <div class="wpb_wrapper"><p style="text-align: left;">
                                                                        <strong>02.</strong> We do other things.</p>
                                                                </div>
                                                            </div>
                                                            <div class="vcex-spacing" style="height:30px"></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper"><p>Phasellus
                                                                        ut pharetra arcu eu tellus tincidunt, ac rutrum
                                                                        risus consectetur.
                                                                        Morbi rhoncus diam molestie tempor vestibulum.
                                                                        Sed
                                                                        ut sem purus. Vivamus
                                                                        quam justo, elementum eu libero in, interdum
                                                                        vestibulum ex.</p>
                                                                    <p>Suspendisse potenti. Cras blandit sagittis
                                                                        maximus et donec. Nulla
                                                                        rutrum eget arcu ac placerat. Quisque sagittis,
                                                                        dui sit amet condimentum
                                                                        blandit, ante est auctor odio.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_left-to-right wpb_start_animation">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_single_image-wrapper"><img
                                                                                src="images/welcome/mobile.png"
                                                                                class="vc_single_image-img attachment-full"
                                                                                alt="" width="635" height="425"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpex-vc-row-wrap clr wpex-vc-row-centered wpex-vc-has-custom-column-spacing wpex-vc-column-spacing-40 wpex-offset-vc-20">
                                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1477936666575">
                                        <div class="container center-row clr">
                                            <div class="wpex-vc-columns-wrap clr">
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper"><p style="text-align: left;">
                                                                        <span style="font-size: 28px;"><strong>03.</strong> We even more&nbsp;things.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="vcex-spacing" style="height:30px"></div>
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper"><p>Maximus bibendum augue, sed
                                                                        tincidunt dui eleifend non.</p>
                                                                    <p>Pellentesque habitant morbi tristique senectus et
                                                                        netus et malesuada
                                                                        fames ac turpis egestas. Ut hendrerit
                                                                        scelerisque dignissim. Suspendisse
                                                                        potenti. Donec viverra, nulla ut porttitor
                                                                        porta, elit justo bibendum
                                                                        tortor.</p>
                                                                    <p>Phasellus a massa blandit, gravida dui non,
                                                                        hendrerit nulla.
                                                                        Suspendisse lobortis metus semper erat
                                                                        vulputate, quis pulvinar sem
                                                                        aliquam. Etiam commodo sed erat eget venenatis.
                                                                        Praesent bibendum
                                                                        rhoncus consequat. Mauris mattis viverra mi.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner wpex-clr">
                                                        <div class="wpb_wrapper wpex-vc-column-wrapper wpex-clr ">
                                                            <div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left wpb_start_animation">
                                                                <div class="wpb_wrapper">
                                                                    <div class="vc_single_image-wrapper"><img
                                                                                src="images/welcome/desk.png"
                                                                                class="vc_single_image-img attachment-full"
                                                                                alt="" width="653" height="526"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>

                    </div><!-- #content -->


                </div><!-- #primary -->


            </div><!-- .container -->


        </main><!-- #main-content -->


        <div id="footer-callout-wrap" class="clr">

            <div id="footer-callout" class="clr container">


                <div id="footer-callout-left" class="footer-callout-content clr">Contact us for a custom quote. We'd
                    love to work with your team!
                </div>


                <div id="footer-callout-right" class="footer-callout-button wpex-clr">
                    <a href="http://totaltheme.wpengine.com/webdevelopers/contact/" class="theme-button"
                       title="Get In Touch">Get In Touch<span
                                class="theme-button-icon-right fa fa-angle-double-right"></span></a>
                </div>


            </div>

        </div>


    </div><!-- #wrap -->


</div><!-- .outer-wrap -->


<div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close" aria-hidden="true"></a></div>
<script type="text/javascript">
    /* <![CDATA[ */
    var wpexLocalize = {
        "isRTL": "",
        "mainLayout": "full-width",
        "menuSearchStyle": "disabled",
        "siteHeaderStyle": "one",
        "megaMenuJS": "1",
        "superfishDelay": "600",
        "superfishSpeed": "fast",
        "superfishSpeedOut": "fast",
        "hasMobileMenu": "1",
        "mobileMenuBreakpoint": "960",
        "mobileMenuStyle": "sidr",
        "mobileMenuToggleStyle": "icon_buttons",
        "localScrollUpdateHash": "",
        "localScrollSpeed": "1000",
        "localScrollEasing": "easeInOutExpo",
        "scrollTopSpeed": "1000",
        "scrollTopOffset": "100",
        "carouselSpeed": "150",
        "customSelects": ".woocommerce-ordering .orderby, #dropdown_product_cat, .widget_categories select, .widget_archive select, #bbp_stick_topic_select, #bbp_topic_status_select, #bbp_destination_topic, .single-product .variations_form .variations select",
        "overlaysMobileSupport": "1",
        "hasStickyHeader": "1",
        "stickyHeaderStyle": "standard",
        "hasStickyMobileHeader": "",
        "overlayHeaderStickyTop": "0",
        "stickyHeaderBreakPoint": "960",
        "sidrSource": "#sidr-close, #site-navigation",
        "sidrDisplace": "",
        "sidrSide": "right",
        "sidrBodyNoScroll": "",
        "sidrSpeed": "300",
        "sidrDropdownTarget": "arrow",
        "altercf7Prealoader": "1",
        "iLightbox": {
            "auto": false,
            "skin": "minimal",
            "path": "horizontal",
            "infinite": true,
            "controls": {"arrows": true, "thumbnail": true, "fullscreen": true, "mousewheel": false},
            "effects": {"loadedFadeSpeed": 50, "fadeSpeed": 500},
            "show": {"title": true, "speed": 200},
            "hide": {"speed": 200},
            "overlay": {"blur": true, "opacity": 0.9},
            "social": {"start": true, "show": "mouseenter", "hide": "mouseleave", "buttons": false}
        }
    };
    /* ]]> */
</script>
<script type="text/javascript" src="js/welcome/total-min.js"></script>
<script type="text/javascript"
        src="js/welcome/js_composer_front.js"></script>
<script type="text/javascript" src="js/welcome/waypoints.js"></script>


<div class="wpex-sidr-overlay wpex-hidden"></div>
<div id="sidr-main" style="transition: right 0.3s ease 0s;" class="sidr right">
    <div class="sidr-inner"><a href="#sidr-close" class="sidr-class-toggle-sidr-close" aria-hidden="true"></a></div>
    <div class="sidr-inner">


        <ul id="sidr-id-menu-main" class="sidr-class-dropdown-menu sidr-class-sf-menu sidr-class-sf-js-enabled">
            {{--@if (Route::has('login'))--}}
            {{--@if (Auth::check())--}}
            {{--<a href="{{ url('/home') }}">Home</a>--}}
            {{--@else--}}
            {{--<a href="{{ url('/login') }}">Login</a>--}}
            {{--<a href="{{ url('/register') }}">Register</a>--}}
            {{--@endif--}}
            {{--@endif--}}
            @if (Route::has('login'))
                @if (Auth::check())
                    <li id="sidr-id-menu-item-65"
                        class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-home sidr-class-current-menu-item sidr-class-page_item sidr-class-page-item-2 sidr-class-current_page_item sidr-class-menu-item-65">
                        <a href="{{ url('/home') }}"><span class="sidr-class-link-inner">Home</span></a>
                    </li>
                @else
                    <li id="sidr-id-menu-item-10"
                        class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-home sidr-class-current-menu-item sidr-class-page_item sidr-class-page-item-2 sidr-class-current_page_item sidr-class-menu-item-10">
                        <a href="{{ url('/login') }}"><span class="sidr-class-link-inner">Login</span></a>
                    </li>
                    <li id="sidr-id-menu-item-67"
                        class="sidr-class-menu-item sidr-class-menu-item-type-post_type sidr-class-menu-item-object-page sidr-class-menu-item-home sidr-class-current-menu-item sidr-class-page_item sidr-class-page-item-2 sidr-class-current_page_item sidr-class-menu-item-67">
                        <a href="{{ url('/register') }}"><span class="sidr-class-link-inner">Register</span></a>
                    </li>
                @endif
            @endif

            <li id="sidr-id-menu-item-52"
                class="sidr-class-local-scroll sidr-class-menu-item sidr-class-menu-item-type-custom sidr-class-menu-item-object-custom sidr-class-current_page_item sidr-class-menu-item-home sidr-class-menu-item-52">
                <a href="http://totaltheme.wpengine.com/webdevelopers/#home-about" data-ls_linkto="#home-about"
                   class=""><span class="sidr-class-link-inner">About Us</span></a></li>
            <li id="sidr-id-menu-item-9"
                class="sidr-class-local-scroll sidr-class-menu-item sidr-class-menu-item-type-custom sidr-class-menu-item-object-custom sidr-class-current_page_item sidr-class-menu-item-home sidr-class-menu-item-9">
                <a href="http://totaltheme.wpengine.com/webdevelopers/#home-what" data-ls_linkto="#home-what"
                   class=""><span class="sidr-class-link-inner">What We Do</span></a></li>


        </ul>

    </div>
</div>
</body>
</html>