<?php

namespace App\Http\Controllers;

use App\Transfer;
use App\User;
use App\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $all = Transfer::all();
        $data['all']=$all;
        return view('transfer/index_transfer',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //tao man hinh khoi tao
        return view('transfer/create_transfer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->hasFile('img')) {
            /*
                xu li up load image
            */
            $destination = 'images';
            $file = $request->file('img');
            $file->move($destination, $file->getClientOriginalName());
            $imgUrl = $destination.'/'.$file->getClientOriginalName();
            
            /*
                xu li du lieu nhan duoc tu form request
            */
            $title = $request->title;
            $describe = $request->describe;
            $purpose = $request->purpose;
            $owner = Auth::user()->id;


            /*
                luu du lieu vao db
            */
            $transfer = new Transfer();
            $transfer->title = $title;
            $transfer->describe = $describe;
            $transfer->purpose = $purpose;
            $transfer->owner = $owner;
            $transfer->imgUrl = $imgUrl;
            $transfer->status = false;
            $transfer->save();
        }

        return ;
        // return Auth::user();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show($transfer_id)
    {
        //
        if($transfer_id=="my_store") {
            /*
                lay cac transfer cua owner 
            */
            $owner = Auth::user()->id;
            $transfers = Transfer::where("owner",$owner)->get();
            
            $data["all"] = $transfers;
            return view("transfer/mystore_transfers",$data);

        }
        else {
            /*
                hien thi chi tiet cua tung product
            */
            $proposes = History::where("transfer_id",$transfer_id)->get();
            foreach ($proposes as $propose) {
                $user_name = DB::table('users')->select("name")
                                               ->where("id",$propose->user_id)
                                               ->get();
                $propose->name = $user_name[0]->name;

            }

            $transfer=Transfer::find($transfer_id);
            


            $data["id"]=$transfer->id;
            $data["title"]=$transfer->title;
            $data["describe"]=$transfer->describe;
            $data["purpose"]=$transfer->purpose;
            $data["owner"]=User::find($transfer->owner);
            $data["imgUrl"]=$transfer->imgUrl;
            $data["proposes"]=$proposes;

            return view("transfer/show_transfer",$data);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit(Transfer $transfer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer)
    {
        //
    }

    public function search(Request $request) {
        // $owner = Auth::user()->id;
        // $transfers = Transfer::where("owner",$owner)->get();
        // echo $transfer;
        $transfers = DB::table('transfer')->select('*')->where('title','like','%'.$request->input('search_text').'%')->get();
        $data['all']=$transfers;
        return view('transfer/index_transfer',$data);
    }
}
