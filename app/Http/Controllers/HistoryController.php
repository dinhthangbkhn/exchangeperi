<?php

namespace App\Http\Controllers;

use App\History;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()) {
            $transfer_ids = DB::table('history')->select('transfer_id')
                                                ->where('user_id',Auth::user()->id)
                                                ->distinct()
                                                ->get();
//            foreach ($transfer_ids as $id) {
//                $mytransfer = DB::table('transfer')->select('*')->where('id',$id->transfer_id)->get();
//            }
            for( $i=0;$i<count($transfer_ids);$i++) {
                $mytransfer[$i] = DB::table('transfer')->select('*')->where('id',$transfer_ids[$i]->transfer_id)->get()[0];
//                echo $mytransfer[$i];
            }
            $data['all']=$mytransfer;
            return view('history/history',$data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_id = Auth::user()->id;
        $transfer_id = $request->transfer_id;
        $propose = $request->propose;
        $success = false;

        $history = new History;
        $history->user_id = $user_id;
        $history->transfer_id = $transfer_id;
        $history->propose = $propose;
        $history->success = $success;
        $history->save();

        return $propose;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function show(History $history)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function edit(History $history)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, History $history)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\History  $history
     * @return \Illuminate\Http\Response
     */
    public function destroy(History $history)
    {
        //
    }
}
