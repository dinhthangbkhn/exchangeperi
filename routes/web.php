<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
// Route::get('/index','TransferController@index');

Route::get('/transfers','TransferController@index');
Route::get('/transfers/create','TransferController@create');
Route::post('/transfers','TransferController@store');
Route::get('/transfers/{transfer_id}','TransferController@show');
Route::get('/transfers/{transfer_id}/edit','TransferController@edit');
Route::put('/transfers/{transfer_id}','TransferController@update');
Route::delete('/transfers/{transfer_id}','TransferController@destroy');
Route::get('/search/transfers','TransferController@search');

Route::get('/historys','HistoryController@index');
Route::post('/historys','HistoryController@store');
Route::get('/historys/comment','HistoryController@comment');

